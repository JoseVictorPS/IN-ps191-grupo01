class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    if user==Direction.find_by(short: 'gp').director
      can :manage, User.where.not(kind: 'admin')
    end

    if Direction.find_by(director: user)
      can :manage, Direction.find_by(director: user)
    end

    if Cellule.find_by(manager_id: user)
      can :manage, Cellule.find_by(manager_id: user)
    end
    if user.admin?
      can :manage, :all
      can :update, :all

    elsif user.manager?

      can :manage, user.cellules 
      can :update, user

    elsif user.director?
      can :manage, user.direction if user.direction.director==user
      can :update, user
      
    else
      can :update, user
      can :read, :all
    end
    if !user.admin?
      can :update, Forum, user_id: user.id
      can :destroy, Forum, user_id: user.id
      can :destroy, Note, user_id: user.id
      can :update, Article, user_id: user.id
      can :destroy, Article, user_id: user.id
      can :destroy, Comment, user_id: user.id
    end
    if user.gp_director?
      can :create, User
      can :update, User
    end
    if user.pro_director? or user.pmo_manager?
      can :create, Project
    end
    
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
