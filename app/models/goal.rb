class Goal < ApplicationRecord
  belongs_to :direction,optional: true
  belongs_to :cellule,optional: true
  belongs_to :company,optional: true
  has_many :tasks,dependent: :destroy
end
