class Skill < ApplicationRecord

  has_many :user_skills
  has_many :users,through: :user_skills
  enum kind:{
    programming: 1,
    management: 2

  }
end
