class Forum < ApplicationRecord
  belongs_to :user

  acts_as_taggable
  has_many :notes, dependent: :destroy

  acts_as_votable
end
