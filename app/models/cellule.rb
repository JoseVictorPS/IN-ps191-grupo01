class Cellule < ApplicationRecord
  validates :name,presence: true
  
  belongs_to :direction, optional: true
  belongs_to :manager, :class_name => 'User', optional: true,:foreign_key => 'manager_id'
  has_many :cellule_users
  has_many :users
  has_many :goals
  
  has_many :history_cellules
end
