class Direction < ApplicationRecord
  validates :name,presence: true
  
  belongs_to :director, :class_name => 'User', optional: true,:foreign_key => 'director_id'
  has_many :cellules;
  has_many :users
  has_many :goals
  has_many :directionusers

  validates :short, uniqueness: true,presence: true

  def to_param
    short
  end
end
