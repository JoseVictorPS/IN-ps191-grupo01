class Project < ApplicationRecord
  validates :name,:description,:condition, presence: true
  
  has_many :history_users
  has_many :user_projects
  has_many :users, through: :user_projects
  belongs_to :director, :class_name => "User",:foreign_key => 'director_id'
  belongs_to :manager, :class_name => "User",:foreign_key => 'manager_id'
  
  has_many :tecnologies
  accepts_nested_attributes_for :tecnologies, allow_destroy: true
  #, allow_destroy: true, reject_if: proc { |attributes| attributes['name'].blank? }

  enum condition:{
    active: 1,
    stopped: 2,
    finished: 3,
    late: 4
  }
end
