class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable

  validates_format_of :email, with: /\A([^@\s]+)@injunior.com\z/i, on: [:create,:update]

  has_many :forums
  has_many :notes
  has_many :articles
  has_many :comments

  acts_as_voter
  
  belongs_to :direction,optional: true
  belongs_to :direction_fusion, :class_name => 'Direction', optional: true,:foreign_key => 'direction_fusion_id'
  
  belongs_to :cellule, optional: true
  belongs_to :creation,:class_name => "Cellule", :foreign_key => 'creation_id',optional: true
  has_many :history_cellules
  
  
  mount_uploader :avatar, AvatarUploader 

  has_many :user_projects
  has_many :projects, through: :user_projects

  has_many :user_skills
  has_many :skills,through: :user_skills
  accepts_nested_attributes_for :skills, allow_destroy: true

  has_many :direction_users


  enum kind:{
    standard: 1,
    manager: 2,
    director: 3, 
    admin: 4,
    gp_director: 5,
    pro_director: 6,
    pmo_manager: 7
  }

  def self.search(search)
    if search!='' and !search.nil?
      where(["name LIKE ?","%#{search}%"])
    else
      all
    end
  end

end
