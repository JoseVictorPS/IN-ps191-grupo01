class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  respond_to :js, :json, :html

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.where("title LIKE ? OR area LIKE ?", "%#{params[:search]}%", "%#{params[:search]}%").order("created_at DESC")
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        call_users(@article)
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def call_users(article)
    @article = article
    @users = User.all
    @users.each do |user|
      UserMailer.article_mail(user,@article).deliver
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def vote
    @article = Article.find(params[:id])
    if !current_user.liked? @article and !current_user.disliked? @article
      @article.upvote_by current_user
    elsif current_user.liked? @article
        @article.unvote_by current_user
    end
    redirect_back fallback_location: @article
  end

  def downvote
    @article = Article.find(params[:id])
    if !current_user.disliked? @article and !current_user.liked? @article
      @article.downvote_by current_user
    elsif current_user.disliked? @article
        @article.unvote_by current_user
    end
    redirect_back fallback_location: @article
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:user_id, :body, :title, :area)
    end
end
