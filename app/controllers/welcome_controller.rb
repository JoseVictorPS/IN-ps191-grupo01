class WelcomeController < ApplicationController
  def home
    @articles = Article.where("title LIKE ? OR area LIKE ?", "%#{params[:search]}%", "%#{params[:search]}%").order("created_at DESC")
  end
end
