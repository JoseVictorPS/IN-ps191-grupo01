class ForumsController < ApplicationController
  before_action :set_forum, only: [:show, :edit, :update, :destroy, :vote]
  respond_to :js, :json, :html

  # GET /forums
  # GET /forums.json
  def index
    if params[:tag]
      @forums = Forum.tagged_with(params[:tag]).order('created_at DESC')
    elsif !params[:search].nil?
      if !params[:search].empty?
        @forums = Forum.tagged_with(params[:search])
      else
        @forums = Forum.all.order('created_at DESC')
      end
    else
      @forums = Forum.all.order('created_at DESC')
    end
  end

  # GET /forums/1
  # GET /forums/1.json
  def show
  end

  # GET /forums/new
  def new
    @forum = Forum.new
  end

  # GET /forums/1/edit
  def edit
  end

  # POST /forums
  # POST /forums.json
  def create
    @forum = Forum.new(forum_params)

    respond_to do |format|
      if @forum.save
        call_users(@forum)
        format.html { redirect_to @forum, notice: 'Forum was successfully created.' }
        format.json { render :show, status: :created, location: @forum }
      else
        format.html { render :new }
        format.json { render json: @forum.errors, status: :unprocessable_entity }
      end
    end
  end

  def call_users(forum)
    @forum = forum
    @users = User.all
    @users.each do |user|
      UserMailer.forum_mail(user,@forum).deliver
    end
  end

  # PATCH/PUT /forums/1
  # PATCH/PUT /forums/1.json
  def update
    respond_to do |format|
      if @forum.update(forum_params)
        format.html { redirect_to @forum, notice: 'Forum was successfully updated.' }
        format.json { render :show, status: :ok, location: @forum }
      else
        format.html { render :edit }
        format.json { render json: @forum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forums/1
  # DELETE /forums/1.json
  def destroy
    @forum.destroy
    respond_to do |format|
      format.html { redirect_to forums_url, notice: 'Forum was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def vote
    if !current_user.liked? @forum and !current_user.disliked? @forum
      @forum.liked_by current_user
    elsif current_user.liked? @forum
      @forum.unvote_by current_user
    end
  end
  
  def downvote
    @forum = Forum.find(params[:id])
    if !current_user.disliked? @forum and !current_user.liked? @forum
      @forum.downvote_by current_user
    elsif current_user.disliked? @forum
        @forum.unvote_by current_user
    end
    redirect_back fallback_location: @forum
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_forum
      @forum = Forum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def forum_params
      params.require(:forum).permit(:user_id, :content, :tag_list)
    end
end
