class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :change, :give_up, :accept]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
    if @user.direction_id
      DirectionUser.create(user_id: @user.id, direction_id: @user.direction_id)
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
    if @user.direction_id
      DirectionUser.create(user_id: @user.id, direction_id: @user.direction_id)
    end
    if @user.cellule_id || @user.creation_id
      
      @history=HistoryCellule.new
      @history.user_id=@user.id
    end
    if @user.cellule_id
      
      @history.cellule_id= @user.cellule_id
    end
    if @user.creation_id
 
      @history.creation_id=@user.creation_id
    end
    if @user.cellule_id || @user.creation_id
     
      @history.save
    end
  end



  def change
    
    
  end

  def give_up
    @user.update_attributes(direction_fusion_id: nil) 
    redirect_to @user
  end

  def accept
    @user.update_attributes(direction_id: @user.direction_fusion_id)
    @user.update_attributes(direction_fusion_id: nil)
    redirect_to @user
  end


  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :kind, :email, :direction_id, :direction_fusion_id,:avatar,{:skill_ids => []},:cellule_id, :creation_id, :password, skills_attributes: [:name, :_destroy, :id, :kind])
    end


    end

