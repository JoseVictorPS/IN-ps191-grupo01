class GoalsController < ApplicationController
  before_action :set_goal, only: [:show, :edit, :update, :destroy]

  # GET /goals
  # GET /goals.json
  def index
    @goals = Goal.all
  end

  # GET /goals/1
  # GET /goals/1.json
  def show
  end

  def confimation
    
    @task=Task.find(params[:id])
    @task.update_attributes(conclusion: true)

    #verificar o estado da meta
    @task.goal.conclusion=true
    for task in @task.goal.tasks
      if !task.conclusion
        @task.goal.conclusion=false
      end
    end
    @task.goal.update_attributes(conclusion: @task.goal.conclusion)
          
    redirect_to @task.goal
  end

  def not_confimation
    @task=Task.find(params[:id])
    
    @task.update_attributes!(conclusion: false)
    @task.goal.update_attributes!(conclusion: false)
    redirect_to @task.goal
  end
  # GET /goals/new
  def new
    @goal = Goal.new
    @direction = Direction.find_by_short!(params[:id])
    @goal.direction_id=@direction.id
  end

  def new_goal
    @goal = Goal.new
    @cellule = Cellule.find(params[:id])
    @goal.cellule_id=@cellule.id
  end

  def new_goal_company
    @goal = Goal.new
    @company = Company.find(params[:id])
    @goal.company_id=@company.id
  end
  # GET /goals/1/edit
  def edit
  end

  # POST /goals
  # POST /goals.json
  def create
    @goal = Goal.new(goal_params)

    respond_to do |format|
      if @goal.save
        format.html { redirect_to @goal, notice: 'Goal was successfully created.' }
        format.json { render :show, status: :created, location: @goal }
      else
        format.html { render :new }
        format.json { render json: @goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /goals/1
  # PATCH/PUT /goals/1.json
  def update
    respond_to do |format|
      if @goal.update(goal_params)
        format.html { redirect_to @goal, notice: 'Goal was successfully updated.' }
        format.json { render :show, status: :ok, location: @goal }
      else
        format.html { render :edit }
        format.json { render json: @goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /goals/1
  # DELETE /goals/1.json
  def destroy
    @goal.destroy
    respond_to do |format|
      format.html { redirect_to goals_url, notice: 'Goal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_goal
      @goal = Goal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def goal_params
      params.require(:goal).permit(:direction_id, :cellule_id, :name, :description,:company_id)
    end
end
