class CellulesController < ApplicationController
  before_action :set_cellule, only: [:show, :edit, :update, :destroy]
  before_action :redirect_not_allowed, except: [:show,:index]


  # GET /cellules
  # GET /cellules.json
  def index
    @cellules = Cellule.all
  end

  # GET /cellules/1
  # GET /cellules/1.json
  def show
  end

  # GET /cellules/new
  def new
    @cellule = Cellule.new
  end

  # GET /cellules/1/edit
  def edit
  end

  # POST /cellules
  # POST /cellules.json
  def create
    @cellule = Cellule.new(cellule_params)

    respond_to do |format|
      if @cellule.save
        format.html { redirect_to @cellule, notice: 'Cellule was successfully created.' }
        format.json { render :show, status: :created, location: @cellule }
      else
        format.html { render :new }
        format.json { render json: @cellule.errors, status: :unprocessable_entity }
      end
    end
    
    
  end

  # PATCH/PUT /cellules/1
  # PATCH/PUT /cellules/1.json
  def update
    respond_to do |format|
      if @cellule.update(cellule_params)
        format.html { redirect_to @cellule, notice: 'Cellule was successfully updated.' }
        format.json { render :show, status: :ok, location: @cellule }
      else
        format.html { render :edit }
        format.json { render json: @cellule.errors, status: :unprocessable_entity }
      end
    end
    
  end

  # DELETE /cellules/1
  # DELETE /cellules/1.json
  def destroy
    @cellule.destroy
    respond_to do |format|
      format.html { redirect_to cellules_url, notice: 'Cellule was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cellule
      @cellule = Cellule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cellule_params
      params.require(:cellule).permit(:name, :direction_id, :manager_id)
    end

    def redirect_not_allowed
      if cannot? :manager, @cellule
        redirect_to root_path
      end
    end

end
