class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]

  # GET /projects
  # GET /projects.json
  def index
    @projects = Project.all.order('updated_at DESC')
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
  end

  # GET /projects/new
  def new
    #redirect_back(fallback_location: fallback_location)
    @project = Project.new
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
    @aux=@project.user_ids
    if @project.manager_id
      @aux.append(@project.manager_id)
    end
    if @project.director_id
      @aux.append(@project.director_id)
    end
    @project.update_attributes(user_ids: @aux)
    @project.update_attributes(start: @project.created_at)
  end



  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    #tecs = Tecnology.where(project_id: @project.id)
    #logies = tecs
    #logies.each do
    #logies.attributes = tecs.attributes
    #tecs.each(&:destroy!)
    #@project.tecnologies.attributes = logies.attributes
    #  @project.t_attributes = {_destroy: '1'}
    #  @project.t.marked_for_destruction?
    #  @project.t.save
    #  @project.t.reload
    #end
    #@project.tecnologies = []
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
    @aux=@project.user_ids
    if @project.manager_id
      @aux.append(@project.manager_id)
    end
    if @project.director_id
      @aux.append(@project.director_id)
    end
    @project.update_attributes(user_ids: @aux)

    if @project.condition=='finished'
      @project.update_attributes(finish: @project.updated_at)
    end

    for user_id in @project.user_ids
      HistoryUser.create(user_id: user_id, project_id: @project.id)
    end

  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:name, :condition, :description, :manager_id, :director_id, {:user_ids => []}, tecnologies_attributes: [:name, :_destroy, :id, :project_id])
    end
end
