class DirectionsController < ApplicationController
  before_action :set_direction, only: [:show, :edit, :update, :destroy]
  before_action :redirect_not_allowed, except: :show

  # GET /directions
  # GET /directions.json
  def index
    @directions = Direction.all
  end

  # GET /directions/1
  # GET /directions/1.json
  def show
  end

  # GET /directions/new
  def new
    @direction = Direction.new
  end

  # GET /directions/1/edit
  def edit
  end

  # POST /directions
  # POST /directions.json
  def create
    @direction = Direction.new(direction_params)
    if @direction.director
      @direction.director.update_attributes(direction_id: @direction.id)
    end
    respond_to do |format|
      if @direction.save
        format.html { redirect_to @direction, notice: 'Direction was successfully created.' }
        format.json { render :show, status: :created, location: @direction }
      else
        format.html { render :new }
        format.json { render json: @direction.errors, status: :unprocessable_entity }
      end
    end
    if @direction.director_id
      DirectionUser.create(user_id: @direction.director.id, direction_id: @direction.id)
    end
  end

  # PATCH/PUT /directions/1
  # PATCH/PUT /directions/1.json
  def update
    respond_to do |format|

      
      if @direction.update(direction_params)
        format.html { redirect_to @direction, notice: 'Direction was successfully updated.' }
        format.json { render :show, status: :ok, location: @direction }
      else
        format.html { render :edit }
        format.json { render json: @direction.errors, status: :unprocessable_entity }
      end
    end
    if @direction.director
      @direction.director.update(direction_id: @direction.id)
    end
    if @direction.director_id
      
      DirectionUser.create(user_id: @direction.director.id, direction_id: @direction.id)
    end
  end

  # DELETE /directions/1
  # DELETE /directions/1.json
  def destroy
    @direction.destroy
    respond_to do |format|
      format.html { redirect_to directions_url, notice: 'Direction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_direction
      @direction = Direction.find_by_short!(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def direction_params
      params.require(:direction).permit(:name, :director_id)
    end

    def redirect_not_allowed
      if cannot? :manager, @direction
        redirect_to root_path
      end
    end
end
