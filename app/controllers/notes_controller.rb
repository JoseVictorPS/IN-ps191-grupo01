class NotesController < ApplicationController
    def create
        @forum = Forum.find(params[:forum_id])
        @note = @forum.notes.create(params[:note].permit(:body, :user_id))
        redirect_to forum_path(@forum)
    end

    def destroy
        @forum = Forum.find(params[:forum_id])
        @note = @forum.notes.find(params[:id])
        @note.destroy
        redirect_to forum_path(@forum)
    end
end
