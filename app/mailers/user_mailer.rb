class UserMailer < ApplicationMailer

    def article_mail(user,article)
        @article = article
        mail(to: user.email, subject: "Novo Artigo em INjunior")
    end

    def forum_mail(user,forum)
        @forum = forum
        mail(to: user.email, subject: "Novo post de Forum em INjunior")
    end
    
end
