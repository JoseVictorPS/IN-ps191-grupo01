class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@injunior.com'
  layout 'mailer'
end
