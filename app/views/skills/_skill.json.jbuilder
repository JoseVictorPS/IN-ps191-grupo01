json.extract! skill, :id, :name, :description, :kind, :created_at, :updated_at
json.url skill_url(skill, format: :json)
