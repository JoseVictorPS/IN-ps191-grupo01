json.extract! goal, :id, :direction_id, :cellule_id, :name, :description, :created_at, :updated_at
json.url goal_url(goal, format: :json)
