json.extract! task, :id, :goal_id, :name, :description, :conclusion, :created_at, :updated_at
json.url task_url(task, format: :json)
