module ProjectsHelper
  def option_condition_project
    [["Ativo", :active],["Finalizado", :finished],["Parado", :stopped],["Atrasado", :late]]
  end

  def option_translate_project(option)
    if option=='active'
      'Ativo'
    elsif option=='finished'
      'Finalizado'
    elsif option=='stopped'
      'Parado'
    elsif option=='late'
      'Atrasado'
    end
  end
end
