module DirectionsHelper
  def chooses_for_director(direction)
    User.where.not(id: direction.director_id)
  end
end
