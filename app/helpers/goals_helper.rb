module GoalsHelper
  def can_manage_goal?(goal)
    if current_user.admin?
      return true
    end
    
    if goal.direction
      if (goal.direction.director==current_user) 
        return true
      end
    end
    if goal.cellule
      if goal.cellule.manager==current_user
        return true
      end
    end
    return false

  end
end
