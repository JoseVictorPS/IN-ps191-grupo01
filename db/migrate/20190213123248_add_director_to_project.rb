class AddDirectorToProject < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :director_id, :integer
    add_column :projects, :manager_id, :integer
  end
end
