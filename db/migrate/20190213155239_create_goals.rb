class CreateGoals < ActiveRecord::Migration[5.2]
  def change
    create_table :goals do |t|
      t.references :direction, foreign_key: true
      t.references :cellule, foreign_key: true
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
