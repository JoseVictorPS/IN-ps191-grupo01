class AddShortToDirection < ActiveRecord::Migration[5.2]
  def change
    add_column :directions, :short, :string
  end
end
