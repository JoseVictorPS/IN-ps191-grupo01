class CreateHistoryCellules < ActiveRecord::Migration[5.2]
  def change
    create_table :history_cellules do |t|
      t.references :cellule, foreign_key: true
      t.references :user, foreign_key: true
      t.integer :creation_id

      t.timestamps
    end
  end
end
