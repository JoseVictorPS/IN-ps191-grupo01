class AddManagerToCellule < ActiveRecord::Migration[5.2]
  def change
    add_column :cellules, :manager_id, :integer
  end
end
