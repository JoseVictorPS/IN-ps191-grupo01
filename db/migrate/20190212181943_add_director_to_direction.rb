class AddDirectorToDirection < ActiveRecord::Migration[5.2]
  def change
    add_column :directions, :director_id, :integer
  end
end
