class RemoveDescriptionFromCellule < ActiveRecord::Migration[5.2]
  def change
    remove_column :cellules, :description, :text
  end
end
