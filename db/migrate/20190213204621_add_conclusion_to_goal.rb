class AddConclusionToGoal < ActiveRecord::Migration[5.2]
  def change
    add_column :goals, :conclusion, :boolean
  end
end
