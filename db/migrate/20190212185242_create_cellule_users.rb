class CreateCelluleUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :cellule_users do |t|
      t.references :user, foreign_key: true
      t.references :cellule, foreign_key: true

      t.timestamps
    end
  end
end
