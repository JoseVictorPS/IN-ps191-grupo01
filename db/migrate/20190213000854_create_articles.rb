class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.references :user, foreign_key: true
      t.text :body
      t.string :title
      t.string :area

      t.timestamps
    end
  end
end
