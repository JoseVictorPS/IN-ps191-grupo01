class AddCompanyToGoal < ActiveRecord::Migration[5.2]
  def change
    add_reference :goals, :company, foreign_key: true
  end
end
