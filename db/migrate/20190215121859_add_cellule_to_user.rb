class AddCelluleToUser < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :cellule, foreign_key: true
    add_column :users, :creation_id, :integer
  end
end
