class RemoveDescriptionFromDirection < ActiveRecord::Migration[5.2]
  def change
    remove_column :directions, :description, :text
  end
end
