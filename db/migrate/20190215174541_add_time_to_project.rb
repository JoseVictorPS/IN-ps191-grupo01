class AddTimeToProject < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :start, :datetime
    add_column :projects, :finish, :datetime
  end
end
