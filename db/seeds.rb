# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# User.create!(email: 'victor@injunior.com', password: '123456', name: 'Victor')
#User.create!(email: 'paiva@injunior.com', password: '123456', name: 'Paiva')

Direction.create!(name:'Operacoes', short:'op')
Direction.create!(name:'Gestão de Pessoas',short:'gp')
Direction.create!(name:'Projetos',short: 'p')
=begin Direction.create!(name:'Finanças', description: 'abc')
# Direction.create!(name:'Gestão de Pessoas', description: 'bdc')
Direction.create!(name:'Projetos', description: 'cde')
User.create!(name:'admin',kind: 'admin', email:'admin@injunior.com',password: '123456')

User.create!(name:'José',email:'jose@injunior.com',password: '123456')
User.create!(name:'Victor',email:'victor@injunior.com',password: '123456')
User.create!(name:'Paiva',kind:'gp_director',email:'paiva@injunior.com',password: '123456')
=end

# User.create!(name:'Silva',kind:'pro_director',email:'silva@injunior.com',password: '123456')
User.create!(name:'admin',kind: 'admin',email:'admin@injunior.com',password: '123456',direction_id: 1, avatar: "wolf-avatar.jpg")
# User.create!(name:'admin',kind: 'admin',email:'admin1@injunior.com',password: '123456',direction_id: 1, avatar: "wolf-avatar.jpg")
# User.create!(name:'admin',kind: 'admin',email:'admin2@injunior.com',password: '123456',direction_id: 1, avatar: "wolf-avatar.jpg")
# User.create!(name:'admin',kind: 'admin',email:'admin3@injunior.com',password: '123456',direction_id: 1, avatar: "wolf-avatar.jpg")
# User.create!(name:'admin',kind: 'admin',email:'admin4@injunior.com',password: '123456',direction_id: 1, avatar: "wolf-avatar.jpg")
# User.create!(name:'admin',kind: 'admin',email:'admin5@injunior.com',password: '123456',direction_id: 1, avatar: "wolf-avatar.jpg")

Cellule.create!(name:'Criação')
Cellule.create!(name:'Análise e Comercial')
Cellule.create!(name:'Pesquisa e Desenvolvimento')
Cellule.create!(name:'Project Management Office')



# User.create!(name:'Nicole',kind: 'standard',email:'nicole2@injunior.com',password: '123456',direction_id: 1, avatar: "wolf-avatar.jpg")

# Direction.create!(name:'pessoas',description:'bem legal')
# User.create!(name:'admin',kind: 'admin',email:'admin@injunior.com',password: '123456',direction_id: 1)

# FAVOR NÃO APAGAR MEUS SEEDS, COMENTA ELES SE ESTIVEREM TE ATRAPALHANDO. GRATA
# Direction.create(name: 'Finanças')
# Direction.create(name: 'Gestão de Pessoas')
# Direction.create(name: 'Projetos')

# User.create(name: 'Niasi', kind: 'admin', email: 'niasi@injunior.com', password: '123123')
# User.create(name: 'Maria Clara Mussi', email: 'mussi@injunior.com', password: '123123')
User.create(name: 'Niasi', kind: 'admin', email: 'niasi@injunior.com', password: '123123')
User.create(name: 'Maria Clara Mussi', email: 'mussi@injunior.com', password: '123123')
User.create!(name:'Silva',kind:'pro_director',email:'silva@injunior.com',password: '123456')
User.create(name: 'Alpha', kind: 'admin', email: 'alpha@injunior.com', password: '123123')