Rails.application.routes.draw do


  resources :companies
  mount Ckeditor::Engine => '/ckeditor'
  get 'comments/create'

  get 'd/:short/metas/:id/nova', to: 'goals#new',as: :new_direction_goal
  get 'células/:id/metas/:id/nova', to: 'goals#new_goal',as: :new_cellule_goal
  get 'empresa/:id/metas/:id/nova', to: 'goals#new_goal_company',as: :new_company_goal

 
  get 'metas/:id/tarefas/:id/nova', to: 'tasks#new', as: :new_goal_task
  post 'tarefas/:id/confimation',to: 'goals#confimation',as: :confimation_goal_task
  post 'tarefas/:id/not_confirmation', to:'goals#not_confimation',as: :not_confimation_task
  
  resources :tasks, except: [:new,:index]
  resources :goals, except: [:new,:index]
  resources :skills

  root to: "welcome#home"
  
  resources :forums do
    member do
      put "like", to: "forums#vote"
      put "dislike", to: "forums#downvote"
    end
    resources :notes
  end

  resources :projects do
    resources :tecnologies
  end
  resources :cellules
  resources :directions,:path=>'d'



  resources :articles do
    member do
      put "like", to: "articles#vote"
      put "dislike", to: "articles#downvote"
    end
    resources :comments
  end

  get 'users/:id/change', to: 'users#change', as: :change_user
  post 'users/:id/give_up', to: 'users#give_up', as: :give_up_user
  post 'users/:id/accept', to: 'users#accept', as: :accept_user
  devise_for :users
  resources :users
  get 'welcome/home'
  get 'tags/:tag', to: 'forums#index', as: :tag

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :projects, :path =>'projetos'
  resources :cellules, :path=>'celulas'
  resources :directions

end
